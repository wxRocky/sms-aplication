package com.bmsoft.sms;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bmsoft.sms.permission.Callback;
import com.bmsoft.sms.permission.ConfirmDialog;
import com.bmsoft.sms.permission.OnPermissionCallback;
import com.bmsoft.sms.permission.Permission;
import com.bmsoft.sms.permission.PermissionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements SimpleActivityLifecycle.Listener {

    private EditText et_ip;
    private EditText et_name;
    private EditText et_time;
    private ListView listview;
    Button bottom, butPost, clean;
    private String ip;
    private String actualName;
    private String time;
    private List<Result> listData = new ArrayList<>();
    private SMSAdapter adapter;

    private boolean setBottom = false;
    private boolean isLoop = false;
    private boolean isForeground = true;
    private long loopTime = 1000;
    private List<String> Permissions = new ArrayList<>();
    public static final String VIBRATE = Manifest.permission.VIBRATE;  //震动权限
//    public static final String FLASHLIGHT = Manifest.permission;  //震动权限
    Timer timer = null;
    TimerTask timerTask = null;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                String json = (String) msg.obj;
                if (json == null || "".equals(json)) {
                    return;
                } else {
                    JSONObject object = null;
                    Result result = null;
                    try {
                        object = new JSONObject(json);
                        result = new Result();
                        result.setTime(System.currentTimeMillis());
                        result.setCode(object.optInt("code"));
                        result.setMessage(object.optString("message"));
                        if (object.has("result")) {
                            JSONObject obj = object.getJSONObject("result");
                            Result.ResultBean resultBean = new Result.ResultBean();
                            resultBean.setMonitorCode(obj.getString("monitorCode"));
                            if (obj.has("monitor")) {
                                JSONArray jsonArray = obj.getJSONArray("monitor");
                                List<Result.ResultBean.MonitorBean> list = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Result.ResultBean.MonitorBean b = new Result.ResultBean.MonitorBean();
                                    JSONObject o = new JSONObject(jsonArray.get(i).toString());
                                    b.setErrorMsg(o.getString("errorMsg"));
                                    list.add(b);
                                }
                                resultBean.setMonitor(list);
                            }
                            result.setResult(resultBean);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (result != null) {
                        if (adapter.getCount() == 100){
                            adapter.remove();
                        }
                        adapter.add(result);
                        if (setBottom) {
                            listview.setSelection(listview.getBottom());
                        }

                        if (!isForeground) {
                            if (!result.getResult().getMonitorCode().equals("4000"))
                                NotificationHelper.sendResidentNoticeType0(MainActivity.this, result.getResult().getMonitorCode(), result.getResult().getMonitor().get(0).getErrorMsg());
//                            NotificationHelper.sendResidentNoticeType0(MainActivity.this, "ssss", result.getMessage());

                        }

                    }

               }

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SimpleActivityLifecycle.get(MainActivity.this).addListener(this);
        Permissions.add(VIBRATE);
        requestPermission();

        et_ip = findViewById(R.id.et_ip);
        et_name = findViewById(R.id.et_name);
        et_time = findViewById(R.id.et_time);
        bottom = findViewById(R.id.bottom);
        butPost = findViewById(R.id.butPost);
        clean = findViewById(R.id.clean);
        listview = findViewById(R.id.listview);

        initData();

    }

    private void initData(){

        adapter = new SMSAdapter(this);
        adapter.initData(listData);
        listview.setAdapter(adapter);
        bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (setBottom) {
                    setBottom = false;
                    bottom.setText("始终置底");
                } else {
                    setBottom = true;
                    bottom.setText("取消置底");
                }
            }
        });
        butPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (butPost.getText().equals("开始测试")) {
                    if (excutePost()) {
                        butPost.setText("停止测试");
                    }
                } else {
                    butPost.setText("开始测试");
                    isLoop = false;
                    if (timerTask != null) {
                        timerTask.cancel();
                        timerTask = null;
                    }
                    if (timer != null) {
                        timer.cancel();
                        timer = null;
                    }
                }
            }
        });
        clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listData.clear();
                adapter.setList(listData);
            }
        });

    }




    public boolean excutePost() {

        ip = et_ip.getText().toString().trim();
        actualName = et_name.getText().toString().trim();
        time = et_time.getText().toString().trim();
        if ("".equals(ip)) {
            Toast.makeText(this, "请填写IP地址", Toast.LENGTH_SHORT);
            return false;
        }
        if ("".equals(actualName)) {
            Toast.makeText(this, "请填写actualName", Toast.LENGTH_SHORT);
            return false;
        }
        if ("".equals(time)) {
            Toast.makeText(this, "请填写轮询时间", Toast.LENGTH_SHORT);
            return false;
        }

        try {
            loopTime = Long.valueOf(time) * 1000;
        } catch (Exception e) {
            Toast.makeText(this, "时间输入格式错误", Toast.LENGTH_SHORT);
        }

        isLoop = true;
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                if (isLoop) {
                    Message message = new Message();
                    message.what = 1;
                    message.obj = HttpUtils.submitPostData("http://" + ip + "/project/rollApplication", actualName, "500");
                    handler.sendMessage(message);
                }
            }
        };
        timer.schedule(timerTask, loopTime, loopTime);
        return true;

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 移除监听
        SimpleActivityLifecycle.get(MainActivity.this).removeListener(this);
    }


    @Override
    public void onBecameForeground() {

        // 切换为前台
        isForeground = true;
    }

    @Override
    public void onBecameBackground() {
        isForeground = false;
        // 切换为后台

    }


    public void requestPermission() {
        PermissionManager.instance().with(this).request(new OnPermissionCallback() {
                                                            @Override
                                                            public void onRequestAllow(String permissionName) {
                                                                for (String s : Permissions) {
                                                                    if (s.equals(permissionName)) {
                                                                        Permissions.remove(s);
                                                                        break;
                                                                    }
                                                                }
                                                                if (Permissions.size() == 0) {
//                                                                    checkUpdate();
                                                                }
                                                            }

                                                            @Override
                                                            public void onRequestRefuse(String permissionName) {
                                                                new ConfirmDialog(MainActivity.this, new Callback() {
                                                                    @Override
                                                                    public void callback(int position) {
                                                                        if (position == 1) {
                                                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                                            intent.setData(Uri.parse("package:" + getPackageName())); // 根据包名打开对应的设置界面
                                                                            startActivity(intent);
                                                                        }
                                                                    }
                                                                }).setContent("暂无读写震动权限\n是否前往设置？").show();
                                                            }

                                                            @Override
                                                            public void onRequestNoAsk(String permissionName) {
                                                                new ConfirmDialog(MainActivity.this, new Callback() {
                                                                    @Override
                                                                    public void callback(int position) {
                                                                        if (position == 1) {
                                                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                                            intent.setData(Uri.parse("package:" + getPackageName())); // 根据包名打开对应的设置界面
                                                                            startActivity(intent);
                                                                        }
                                                                    }
                                                                }).setContent("暂无读写震动权限\n是否前往设置？").show();
                                                            }
                                                        }
                , VIBRATE
        );
    }


}
