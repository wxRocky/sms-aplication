package com.bmsoft.sms;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Title:
 * Description:
 * Author：wx
 * Date：2019/1/4 13:19
 */
public class SMSAdapter extends ArrayListAdapter<Result> {

    private LayoutInflater mInflater;

    public SMSAdapter(Activity context) {
        super(context);
        mInflater = LayoutInflater.from(context); // 生成打气筒
    }

    public void add(Result b) {
        mList.add(b);
        notifyDataSetChanged();
    }

    public void remove(){
        mList.remove(0);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Result result = mList.get(position);
        final ViewHolder mHolder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(R.layout.adapter, parent, false);
            mHolder = new ViewHolder();
            mHolder.tv_time = row.findViewById(R.id.tv_time);
            mHolder.tv_code = row.findViewById(R.id.tv_code);
            mHolder.tv_msg = row.findViewById(R.id.tv_msg);
            mHolder.rl_monitorCode = row.findViewById(R.id.rl_monitorCode);
            mHolder.tv_monitorCode = row.findViewById(R.id.tv_monitorCode);
            mHolder.list_item = row.findViewById(R.id.list_item);
            row.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) row.getTag();
        }
        mHolder.tv_time.setText(getTimeString(result.getTime()));
        mHolder.tv_code.setText(String.valueOf(result.getCode()));
        if(result.getCode() == 200) {
            mHolder.list_item.setVisibility(View.VISIBLE);
            mHolder.rl_monitorCode.setVisibility(View.VISIBLE);
            mHolder.tv_monitorCode.setText(result.getResult().getMonitorCode());
            ResultAdapter adapter = new ResultAdapter(mContext);
            adapter.setList(result.getResult().getMonitor());
            mHolder.list_item.setAdapter(adapter);
        } else {
            mHolder.list_item.setVisibility(View.GONE);
            mHolder.rl_monitorCode.setVisibility(View.GONE);
            mHolder.tv_monitorCode.setText("");

        }
        if(result.getMessage() == null || "".equals(result.getMessage())) {
            mHolder.tv_msg.setText("");
        } else {
            mHolder.tv_msg.setText(result.getMessage());
        }
        return row;
    }

    public class ViewHolder {
        TextView tv_time;
        TextView tv_code;
        TextView tv_msg;
        RelativeLayout rl_monitorCode;
        TextView tv_monitorCode;
        ListView list_item;
    }

    public static String getTimeString(final Long l) {
        try {
            Date d = new Date(l);
            final SimpleDateFormat df = new SimpleDateFormat("HH':'mm':'ss",
                    Locale.getDefault());
            final String sDate = df.format(d);
            return sDate;
        } catch (final Exception e) {
            return null;
        }
    }

}
