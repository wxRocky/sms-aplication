package com.bmsoft.sms;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class NotificationHelper {

    public static String NOTICE_ID_KEY = "cn.bmsoft.sms.NOTICE_ID";
    public static final String ACTION_CLOSE_NOTICE = "cn.bmsoft.sms.closenotice";
    public static final int NOTICE_ID_TYPE_0 = R.string.app_name;


    @TargetApi(16)
    public static void sendResidentNoticeType0(Context context, String title, String content) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Notification notification = null;

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notificationManager.deleteNotificationChannel(NOTICE_ID_KEY);
            NOTICE_ID_KEY = NOTICE_ID_KEY+1;
            @SuppressLint("WrongConstant") NotificationChannel mChannel = new NotificationChannel(NOTICE_ID_KEY, context.getString(R.string.app_name), NotificationManager.IMPORTANCE_MAX);
            mChannel.setDescription("cn.bmsoft.sms.closenotice.notofication");
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
//            mChannel.setSound("","");

            notificationManager.createNotificationChannel(mChannel);
            notification = new Notification.Builder(context)
                    .setChannelId(NOTICE_ID_KEY)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSmallIcon(R.mipmap.ic_launcher).build();

        } else {
            NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_LIGHTS)//设置指示灯  
                    .setDefaults(Notification.DEFAULT_SOUND)//设置提示声音  
                    .setDefaults(Notification.DEFAULT_VIBRATE)//设置震动  
                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setOnlyAlertOnce(true)
                    .setOngoing(true);

//                    .setChannelId(id);//无效
            notification = notificationBuilder.build();
        }
        notificationManager.notify(111123, notification);
    }

    public static int getIconColor() {
        return Color.parseColor("#999999");

    }


    private static String getTime() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.SIMPLIFIED_CHINESE);
        return format.format(new Date());
    }


    public static void clearNotification(Context context, int noticeId) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(noticeId);
    }


}
