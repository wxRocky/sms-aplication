package com.bmsoft.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NoticeBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int noticeId = intent.getIntExtra(NotificationHelper.NOTICE_ID_KEY, -1);
        if(noticeId != -1){
            NotificationHelper.clearNotification(context, noticeId);
        }
    }
}