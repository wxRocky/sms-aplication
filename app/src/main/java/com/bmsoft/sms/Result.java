package com.bmsoft.sms;

import java.util.List;

/**
 * Title:
 * Description:
 * Author：wx
 * Date：2019/1/4 13:19
 */
public class Result {


    /**
     * result : {"monitorCode":"4001","monitor":[{"errorMsg":"用户18267608880:短信不可达"}]}
     * code : 200
     * message : 请求成功
     */

    private ResultBean result;
    private long time;
    private int code;
    private String message;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class ResultBean {
        /**
         * monitorCode : 4001
         * monitor : [{"errorMsg":"用户18267608880:短信不可达"}]
         */

        private String monitorCode;
        private List<MonitorBean> monitor;

        public String getMonitorCode() {
            return monitorCode;
        }

        public void setMonitorCode(String monitorCode) {
            this.monitorCode = monitorCode;
        }

        public List<MonitorBean> getMonitor() {
            return monitor;
        }

        public void setMonitor(List<MonitorBean> monitor) {
            this.monitor = monitor;
        }

        public static class MonitorBean {
            /**
             * errorMsg : 用户18267608880:短信不可达
             */

            private String errorMsg;

            public String getErrorMsg() {
                return errorMsg;
            }

            public void setErrorMsg(String errorMsg) {
                this.errorMsg = errorMsg;
            }
        }
    }
}
