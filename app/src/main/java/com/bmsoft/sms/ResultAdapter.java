package com.bmsoft.sms;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Title:
 * Description:
 * Author：wx
 * Date：2019/1/4 15:48
 */
public class ResultAdapter  extends ArrayListAdapter<Result.ResultBean.MonitorBean> {

    private LayoutInflater mInflater;

    public ResultAdapter(Activity context) {
        super(context);
        mInflater = LayoutInflater.from(context); // 生成打气筒
    }

    public void add(Result.ResultBean.MonitorBean b) {
        mList.add(b);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Result.ResultBean.MonitorBean bean = mList.get(position);
        final ViewHolder mHolder;
        View row = convertView;
        if (row == null) {
            row = mInflater.inflate(R.layout.result_adapter, parent, false);
            mHolder = new ViewHolder();
            mHolder.tv_msg = row.findViewById(R.id.tv_errorMsg);
            row.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) row.getTag();
        }
        mHolder.tv_msg.setText(bean.getErrorMsg());
        return row;
    }

    public class ViewHolder {
        TextView tv_msg;
    }

}
