package com.bmsoft.sms;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public abstract class ArrayListAdapter<T> extends BaseAdapter {

    public List<T> mList = new ArrayList<T>();
    public Activity mContext;
    public ListView mListView;
    // 列表标示
    protected String mKey;

    public ArrayListAdapter(Activity context) {
        this.mContext = context;
    }

    public int getCount() {
        if (mList != null)
            return mList.size();
        else
            return 0;
    }

    public Object getItem(int position) {
        return mList == null ? null : mList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    abstract public View getView(int position, View convertView, ViewGroup parent);

    public void setList(List<T> list) {
        mList = list;

        notifyDataSetChanged();
    }

    /**
     * 指定位置插入数据
     * 
     * @author hjl
     * 
     * @param position
     *            待插入的位置
     * @param t
     *            插入的数据实体
     * */
    public void insertDataAtPosition(int position, T t) {
        if (t == null) {
            return;
        }

        if (position >= mList.size()) {
            mList.add(t);
        } else {
            mList.add(position, t);
        }

        notifyDataSetChanged();
    }

    /**
     * 初始化列表数据
     * 
     * @author hjl
     * @param mLists
     *            初始化的数据集合
     * */
    public void initData(List<T> mLists) {
        if (null == mLists || mLists.size() == 0) {
            return;
        }
        mList.clear();
        mList.addAll(mLists);
      notifyDataSetChanged();
      //  notify();
    }

    /**
     * 加载更多数据
     * 
     * @author hjl
     * @param mLists
     *            待加载更多的数据集合
     * */
    public void loadMoreData(List<T> mLists) {
        if (null == mLists || mLists.size() == 0) {
            return;
        }

        mList.addAll(mLists);
        notifyDataSetChanged();
    }

    /**
     * 清空数据
     * */
    public void clearData() {
        mList.clear();
        notifyDataSetChanged();
    }

    public List<T> getList() {
        return mList;
    }

    public void setList(T[] list) {
        ArrayList<T> arrayList = new ArrayList<T>(list.length);
        for (T t : list) {
            arrayList.add(t);
        }
        setList(arrayList);
    }

    public ListView getListView() {
        return mListView;
    }

    public void setListView(ListView listView) {
        mListView = listView;
    }

}